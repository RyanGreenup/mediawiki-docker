FROM mediawiki:latest

# Dependencies .................................................................

# Install dependencies + fish
RUN apt update -y; apt upgrade -y; apt install -y ghostscript neovim fish nodejs npm git

# Math..........................................................................
# Mathoid ----------------------------------------------------------------------
# Install the math server
RUN npm install -g mathoid
# Download the mathoid config
RUN curl https://gitlab.wikimedia.org/repos/releng/mathoid/-/raw/master/config.dev.yaml > config.dev.yaml
# Mathjax ----------------------------------------------------------------------
# --recursive downloads all the mathjax locally, allowing offline access
RUN git clone --recursive https://github.com/jmnote/SimpleMathJax.git extensions/SimpleMathJax

# Install Composer ............................................................
# See:
# * https://getcomposer.org/download/
# Needed for Semantic Mediawiki Later
RUN cwd=$(pwd) && \
    wd=/tmp/download-composer && \
    mkdir -p $wd && \
    cd $wd && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/

# Install CirrusSearch ........................................................
RUN cd extensions/                                                             && \
    git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CirrusSearch && \
    cd CirrusSearch                                                            && \
    composer.phar install --no-dev                                             && \
    cd ..                                                                      && \
    cd ..                                                                      && \
    curl 'https://extdist.wmflabs.org/dist/extensions/Elastica-REL1_41-324bd77.tar.gz' > /tmp/Elastica.tar.gz && \
    tar -xzf /tmp/Elastica.tar.gz -C ./extensions

# Additional Extensions ........................................................
# Embed Video___________________________________________________________________
RUN apt install -y ffmpeg && \
    git clone 'https://gitlab.com/hydrawiki/extensions/EmbedVideo' extensions/EmbedVideo
RUN git clone 'https://gitlab.com/hydrawiki/extensions/PDFEmbed' extensions/PDFEmbed

# Additional Skins .........................................
# These are all the wikiforge skins
RUN \
    cd skins                                                                                && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/apex'             Apex        && \
#   git clone 'https://github.com/ProfessionalWiki/chameleon.git'               Chameleon   && \
    git clone 'https://github.com/StarCitizenTools/mediawiki-skins-Citizen.git' Citizen     && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/CologneBlue'      CologneBlue && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Cosmos'           Cosmos      && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/DuskToDawn'       DuskToDawn  && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/erudite'          Erudite     && \
#   git clone 'https://github.com/femiwiki/FemiwikiSkin.git'                    Femiwiki    && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Gamepress'        Gamepress   && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Mask'             Mask        && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Metrolook'        Metrolook   && \
#   git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Mirage'           Mirage      && \
#   git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Modern'           Modern      && \
#   git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Nostalgia'        Nostalgia   && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Pivot'            Pivot       && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Refreshed'        Refreshed   && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Splash'           Splash      && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/WMAU'             WMAU        && \
    git clone 'https://gitweb.gentoo.org/sites/wiki/skin-tyrian.git'            Tyrian      && \
    git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/Material'         Material    && \
    git clone 'https://bitbucket.org/wikiskripta/medik'                         Medik       && \
#   git clone 'https://gerrit.wikimedia.org/r/mediawiki/skins/BlueSky'          BlueSky     && \
    git clone 'https://github.com/thaider/Tweeki/'                              Tweeki      && \
    git clone 'https://github.com/wikimedia/mediawiki-skins-Anisa/'             Anisa

# SMW ..........................................................................
# Semantic Mediawiki must be last to run update.php on a LocalSettings
# that has all the called extensions
# See also:
# * https://getcomposer.org/download/
# * https://www.semantic-mediawiki.org/wiki/Help:Installation/Quick_guide
# Remember to enable in the LocalSettings.php with:
#
#   wfLoadExtension( 'SemanticMediaWiki' );
#   enableSemantics( $wgServer );
#
# This assumes composer in /usr/local/bin/composer.phar from above
RUN \
    COMPOSER=composer.local.json php /usr/local/bin/composer.phar require --no-update \
        mediawiki/semantic-media-wiki  \
        mediawiki/semantic-bundle      \
        professional-wiki/network   && \
    composer.phar update --no-dev
RUN git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/HierarchyBuilder extensions/HierarchyBuilder
RUN git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CognitiveProcessDesigner extensions/CognitiveProcessDesigner


# Update the Database...........................................................

# SMW requires the database be modified and registered with it
# Commment out everything below If building without
#   * LocalSettings.php
#   * sqlite_db
# Once those are available re run this build script

# Temporarily Copy in the database and settings
    #<<<<<<<  RUN  mv /var/www/data /var/www/data.bak
    #<<<<<<<  COPY sqlite_db /var/www/data
    #<<<<<<<  COPY LocalSettings.php /var/www/html/LocalSettings.php
    #<<<<<<<  # Run the update
    #<<<<<<<  RUN php maintenance/run.php update.php --quick --skip-external-dependencies && \
    #<<<<<<<      echo "\n\n database updated successfully\n\n"
    #<<<<<<<  # # Remove the settings and db
    #<<<<<<<  RUN rm -rf /var/www/data LocalSettings.php && mv /var/www/data.bak /var/www/data




# Rejected Extensions ..........................................................
# Popups -----------------------------------------------------------------------
# This extension doesn't work for me, gives white screen of death

# RUN cd extensions && \
#     rm -rf Popups TextExtracts PageImages && \
#     git clone 'https://gerrit.wikimedia.org/r/mediawiki/extensions/TextExtracts' \
#       TextExtracts && \
#     git clone 'https://gerrit.wikimedia.org/r/mediawiki/extensions/PageImages' \
#       PageImages && \
#     git clone 'https://gerrit.wikimedia.org/r/mediawiki/extensions/Popups' \
#       Popups && \
#     cd Popups          && composer.phar install --no-dev && \
#     cd ../TextExtracts && composer.phar install --no-dev && \
#     cd ../PageImages   && composer.phar install --no-dev

