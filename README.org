* Mediawiki Docker
** Introduction
This is a Dockerfile that will build Mediawiki with:

+ Self Hosted Mathjax
+ Semantic Mediawiki
+ Cirrus Search

** Usage / Setup

1. Clone the repo
   #+begin_src bash
   git clone https://gitlab.com/RyanGreenup/mediawiki-docker
   #+end_src
2. Move the LocalSettings, this is just an exemplar I use personally
   #+begin_src bash
mv LocalSettings.php LocalSettings.php
   #+end_src
3. Note the commented section of the Dockerfile
4. Note the commented section =# <<<<<<= in the =Dockerfile=, leave it commented out until step 9, unless you already have a database and =LocalSettings.php= already in the directory.
5. Build the image
   #+begin_src bash
   docker-compose build
   #+end_src
6. Start the image and follow the install at =localhost:8076=
   #+begin_src bash
   docker-compose up -d
   #+end_src
7. Download the =LocalSettings.php= and place it next to the =docker-compose.yml=
8. Restart the container and verify the wiki works
   #+begin_src bash
   docker-compose down
   docker-compose up -d
   #+end_src
9. Update the Database for Semantic mediawiki, this can be done per container or at build time
   1. Per Container
      #+begin_src bash
      docker-compose up -d
      docker-compose exec wiki php maintenance/update.php --skip-external-dependencies --quick
      #+end_src
   2. At build time
      1. Uncomment the section of the Dockerfile marked with =#<<<<<<<= like so:
         #+begin_src dockerfile
         # Update the Database...........................................................

         # SMW requires the database be modified and registered with it
         # Commment out everything below If building without
         #   * LocalSettings.php
         #   * sqlite_db
         # Once those are available re run this build script

         # Temporarily Copy in the database and settings
         RUN  mv /var/www/data /var/www/data.bak
         COPY sqlite_db /var/www/data
         COPY LocalSettings.php /var/www/html/LocalSettings.php
         # Run the update
         RUN php maintenance/run.php update.php --quick --skip-external-dependencies && \
             echo "\n\n database updated successfully\n\n"
         # # Remove the settings and db
         RUN rm -rf /var/www/data LocalSettings.php && mv /var/www/data.bak /var/www/data
         #+end_src
      2. Verify that the =LocalSettings.php= and =sqlite_db= are in the directory
         #+begin_src bash
         if [ -f LocalSettings.php ] & [ -d sqlite_db ]; then
             doas docker-compose build
         else
             echo "This step expects both LocalSettings.php and sqlite_db"
         fi
         #+end_src
10. Add Extensions to =LocalSettings.php=
    #+begin_src php
    /*
    MATHJAX ---------------------------------------------------------
    Mathjax is the simpler approach, however, it requires downloading
    200 MB of JS external to the container
    */
    wfLoadExtension( 'SimpleMathJax' );
    $wgSmjUseCDN = false;
    $wgSmjScale = 1.05;
    $wgSmjExtraInlineMath = [ [ "$", "$" ], [ "\\(", "\\)" ] ];


    # Use PDFEmbed
    wfLoadExtension( 'PDFEmbed' );
    // Default width for the PDF object container.
    $wgPdfEmbed['width'] = 800;
    // Default height for the PDF object container.
    $wgPdfEmbed['height'] = 1090;
    //Allow user the usage of the pdf tag
    $wgGroupPermissions['*']['embed_pdf'] = true;


    // Add Skins
    wfLoadSkins( [
        'Apex',
    //    'Chameleon',
        'Citizen',
    //    'CologneBlue',
        'Cosmos',
        'DuskToDawn',
        'Erudite',
    //    'Femiwiki',
        'Gamepress',
        'Mask',
        'Metrolook',
     //   'Mirage',
    //    'Modern',
    //    'Nostalgia',
        'Pivot',
        'Refreshed',
        'Splash',
        'WMAU',
        'Tyrian',
        'Material',
        'Medik',
    //    'BlueSky',
        'Tweeki',
        'Anisa'

    ] );

    /* Semantic Mediawiki */

    # If enabling these, you must run :
    #     php maintenance/run.php update.php
    # This is necessary every time the image is rebuilt
    wfLoadExtension( 'SemanticMediaWiki' );
    require_once __DIR__ . '/extensions/SemanticBundle/SemanticBundle.php';
    enableSemantics( $wgServer );
    # SMW Extensions
    wfLoadExtension( 'CognitiveProcessDesigner' );
    wfLoadExtension( 'HierarchyBuilder' );
    #+end_src
11. Look through [[#tips][Tips and Tricks]] below.
12. Look through the [[file:LocalSettings.php]] for any missed settings

*** Prequisites
**** Container
***** Install and Start Docker
****** Gentoo
#+begin_src bash
# Install
emerge app-containers/docker app-containers/docker-compose
# Start
rc-update add docker default
# Enable
rc-service start docker
#+end_src
****** Arch
#+begin_src bash
# Install
pacman -S docker docker-compose
# Start and Enable
systemctl enable --now docker
#+end_src
****** Void
#+begin_src bash
# Install
xbps-install docker docker-compose
# Start and Enable
ln -s /etc/sv/docker /var/services/
#+end_src
***** Podman
Podman could probably be used as well with something like [[https://github.com/containers/podman-compose][podman-compose]]. If you're on an selinux system, it may be necessary to set use the =:z= option, disable selinux or mark the container as privileged, see also the =userns_mode=. An example of all these settings are included below:

#+begin_src yaml
services:
  mediawiki:
    build: ./
    userns_mode: keep-id
    privileged: true
    security_opt:
      - "label=disable"
    volumes:
      - ./sqlite_db:/var/www/data:z

#+end_src
*** Tips and Tricks
:PROPERTIES:
:CUSTOM_ID: tips
:END:
**** ForwardLinks
***** Scribunto Module:
Create the page =Module:ForwardLink= and add the following

#+begin_src lua
local p = {}

function p.h(frame)
    local current_title = mw.title.getCurrentTitle().text
    local page_obj = mw.title.new(current_title)

    -- Fetch the content
    local content = page_obj:getContent()

    -- Table to store extracted links
    local links = {}

    -- Extract internal links: [[Link]]
    for link in string.gmatch(content, "%[%[([^%[%]|]+)") do
    	-- link contains the page name, wrap it back in double square brackets
    	-- to make it a link again
    	local link = "* [[" .. link .. "]]"
        table.insert(links, link)
    end

    --[[
    TODO: Modify this so it extracts the name URL domain name AND the url
    --]]
    -- Extract external links: [http://example.com]
    for link in string.gmatch(content, "%[(http[s]?://[^%s%]]+)") do
    	-- link contains just the url
    	local link = "* " .. link
        table.insert(links, link)
    end

    -- Concatenate links into a string to return
    return table.concat(links, "\n")

end

return p
#+end_src
***** Template
Create the page =Template:ForwardLink=

#+begin_src mediawiki
== Forward Links ==


<div class="toccolours mw-collapsible" style="width:400px; overflow:auto;">
{{#invoke:ForwardLink|h}}
</div>
#+end_src
***** Create a Documentation Page
Create the Page =Forward Links=:


#+begin_src mediawiki
== Resources ==
[[:Module:ForwardLink]]
[[:Template:ForwardLink]]

== Usage ==
{{ForwardLinks}}
#+end_src


**** Automatically Embed Backlinks or ForwardLinks
Add the following to =LocalSettings.php= to embed Backlinks on every page:
#+begin_src php
// Embed Backlinks


$wgHooks['SkinAfterContent'][]='backlinks_func';

function backlinks_func( &$data, Skin $skin ) {
    $title = $skin->getTitle()->getPrefixedText();
    $wikitext = "{{Special:WhatLinksHere/{$title}}}";

    /*Uncomment to include forward links
    //  move h2 down into the out section if you do this
    // I only used it here for evaluation sake
    $wikitext = $wikitext . "<h2> Forward </h2> \n {{ForwardLinks}}";
     */

    $parserOptions = ParserOptions::newFromUser( $skin->getUser() );
    $parsedText = MediaWiki\MediaWikiServices::getInstance()->getParser()->parse( $wikitext, $skin->getTitle(), $parserOptions )->getText();

    $out = '<div id="backlinks" class="backlinks"><h2>Backlinks</h2>'. $parsedText .'</div>';

    $data = $out . $data;
    return true;
}


#+end_src

***** Alternatively, a Template
One can use a template, I found this to be a nuisance though. Much easier to have mediawiki embed the backlinks on every page.

Nonetheless, to create a template add the following to =[[Template:End Article]]=:

#+begin_src mediawiki

<!---
{{ForwardLinks}}
--->


== Backlinks ==
{{Special:WhatLinksHere/{{FULLPAGENAME}}}}

== References ==
<references />

#+end_src
**** TODO List Subpages
** Ongoing Work
*** TODO Add ElasticSearch instance
#+begin_src yaml
# version: '3.3'
services:
    kibana:
       ports:
           - '5601:5601'
#      volumes:
#        - ./kibana.yml:/usr/share/kibana/config/kibana.yml
       restart: always
           # image: docker.elastic.co/kibana/kibana:7.4.0
       image: docker.elastic.co/kibana/kibana:7.10.2
       environment:
          - ELASTICSEARCH_HOSTS=http://elasticsearch:9200    # address of elasticsearch docker container which kibana will connect
       depends_on:
          - elasticsearch
    elasticsearch:
        ports:
            - '9200:9200'
        environment:
          - "discovery.type=single-node"
        restart: always
#+end_src

#+begin_src php
$wgCirrusSearchServers = array( '10.7.143.20' );
$wgSearchType = 'CirrusSearch';
#+end_src
